#!/bin/sh

echo "Instalando Node para Raspberry Pi 4"
sudo apt-get update
sudo apt-get upgrade
wget https://nodejs.org/dist/v14.17.0/node-v14.17.0-linux-arm64.tar.xz
tar xvf node-v14.17.0-linux-arm64.tar.xz
cd node-v14.17.0-linux-arm64
cp -R * /usr/local/
node -v
npm -v