#!/bin/sh

cd /home/sts/sts-raspberry
sudo pm2 stop ecosystem.config.js
git pull
npm install
sudo pm2 start ecosystem.config.js
