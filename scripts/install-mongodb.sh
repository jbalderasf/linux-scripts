#!/bin/sh

curl -fsSL https://www.mongodb.org/static/pgp/server-4.4.asc | sudo apt-key add -
echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu focal/mongodb-org/4.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.4.list
sudo apt update
sudo apt install mongodb-org
sudo systemctl start mongod.service
sudo systemctl status mongod
sudo systemctl enable mongod
sudo sed -i '24s/.*/  0.0.0.0/' /etc/mongod.conf
sudo sed -i '31s/.*/security/' /etc/mongod.conf
sudo sed -i '31 a #' /etc/mongod.conf
sudo sed -i '32s/.*/  authorization: enabled/' /etc/mongod.conf
sudo systemctl restart mongod
echo 'db.createUser({user: "root",pwd: "123456",roles: [ { role: "userAdminAnyDatabase", db: "admin" }, "readWriteAnyDatabase" ]})' | mongo admin