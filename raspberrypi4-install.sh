#!/bin/sh

sudo apt update
sudo apt-get update
sudo apt-get upgrade
echo "Instalando Node para Raspberry Pi 4"
sudo apt-get update
sudo apt-get upgrade
wget https://nodejs.org/dist/v14.17.0/node-v14.17.0-linux-arm64.tar.xz
tar xvf node-v14.17.0-linux-arm64.tar.xz
cd node-v14.17.0-linux-arm64
sudo cp -R * /usr/local/
node -v
npm -v
cd ..
echo "Instalando ssh"
sudo apt install openssh-server
sudo systemctl status ssh
echo "Instalando Git"
sudo apt install git
git --version
echo "Instalando Curl"
sudo apt  install curl
echo "Instalando linux dev kit"
sudo apt-get install build-essential libssl-dev
sudo npm install -g node-gyp --unsafe-perm=true
sudo npm install --unsafe-perm=true
echo "Instalando PM2"
sudo npm install -g pm2@latest --unsafe-perm=true
echo "Instalando MongoDB"
curl -fsSL https://www.mongodb.org/static/pgp/server-4.4.asc | sudo apt-key add -
echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu focal/mongodb-org/4.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.4.list
sudo apt update
sudo apt install mongodb-org
sudo systemctl start mongod.service
sudo systemctl status mongod
sudo systemctl enable mongod
git clone https://gitlab.com/jbalderasf/linux-scripts.git